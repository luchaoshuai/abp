﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BXJG.Equipment
{
    public static class Utils
    {
        ///// <summary>
        ///// 根据设备通信协议，检查和校验，若失败则抛异常
        ///// </summary>
        ///// <param name="buffer">去首尾的字节数组</param>
        //public static void CheckCalculateAdd(this ReadOnlySpan<byte> buffer)
        //{
        //    //经过测试用Slice(-1)取尾部会报错 buffer[-1]也不行
        //    if (!buffer.CalculateAdd().Equals(buffer[buffer.Length - 1]))
        //        throw new Exception($"校验失败！数据：{buffer.To16String()}");
        //}
        ///// <summary>
        ///// 计算校验值
        ///// </summary>
        ///// <param name="buffer">不包含头尾的字节数组</param>
        ///// <returns></returns>
        //public static byte CalculateAdd(this ReadOnlySpan<byte> buffer)
        //{
        //    //和校验位 = （头+命令+长度+协议内容）&  0xff
        //    int cks = 0xbb;//使用supersocket的头尾协议模板定义的协议会掐头去尾，但是我们的协议要求头部字节参与和校验计算
        //    for (int i = 0; i < buffer.Length - 1; i++)
        //    {
        //        cks += buffer[i];
        //    }
        //    var q = cks & 0xff;
        //    return (byte)q;
        //}
        /// <summary>
        /// true 1  false 0
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        public static byte ToByte(this bool b)
        {
            return (byte)(b ? 1 : 0);
        }
        public static bool ToBoolean(this byte b) => b == 1;
        /// <summary>
        /// 转换为16进制的字符串格式
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string ToHexStr(this ReadOnlySpan<byte> bytes)
        {
            //bytes.ToArray()低效
            return bytes.ToArray().ToHexStr();
        }
        /// <summary>
        /// 转换为16进制的字符串格式
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string ToHexStr(this byte[] bytes)
        {
            //bytes.ToArray()低效
            return BitConverter.ToString(bytes).Replace("-", string.Empty);//.ToLower()
        }
        ///// <summary>
        ///// 字节数组转16进制字符串
        ///// </summary>
        ///// <param name="bytes"></param>
        ///// <returns></returns>
        //public static string byteToHexStr(byte[] bytes)
        //{
        //    string returnStr = "";
        //    if (bytes != null)
        //    {
        //        for (int i = 0; i < bytes.Length; i++)
        //        {
        //            returnStr += bytes[i].ToString("X2");
        //        }
        //    }
        //    return returnStr;
        //}
    }
}
