﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BXJG.WeChat.Common
{
   public  class Consts
    {
        public const string WeChatMiniProgramHttpClientName = "WeChatMiniProgramHttpClientName";

        public const string AccessTokenUrl = "https://api.weixin.qq.com/cgi-bin/token";

        public const string TemplateMessageClaimType = "TemplateIdList";
    }
}
