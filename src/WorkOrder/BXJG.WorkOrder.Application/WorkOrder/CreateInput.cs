﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BXJG.WorkOrder.WorkOrder
{
    /// <summary>
    /// 工单后台管理编辑模型<br />
    /// 不同工单类型有相应子类
    /// </summary>
    public class WorkOrderCreateInput : WorkOrderUpdateInput
    {
    }
}
