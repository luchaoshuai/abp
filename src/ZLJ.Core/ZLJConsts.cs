﻿namespace ZLJ
{
    public class ZLJConsts
    {
        public const string LocalizationSourceName = "ZLJ";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
